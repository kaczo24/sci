const fs = require("fs");

const komputery = fs.readFileSync("komputery.txt",{encoding:'utf8', flag:'r'}).split("\n").slice(1, -1).map(x=>x.split("\t"))
const awarie = fs.readFileSync("awarie.txt",{encoding:'utf8', flag:'r'}).split("\n").slice(1, -1).map(x=>x.split("\t"))
const naprawy =  fs.readFileSync("naprawy.txt",{encoding:'utf8', flag:'r'}).split("\n").slice(1, -1).map(x=>x.split("\t"))
let obj = {};
komputery.forEach(x=>obj[x[2]] = (obj[x[2]] ?? 0) + 1);
console.log([["Pojemność", "Liczba"],...Object.entries(obj).sort((b,a)=>a[1] - b[1]).slice(0, 10)]);

let awarieObj = {};
awarie.filter(x=>komputery[+x[1]-1][1] == "A").forEach(x=>awarieObj[x[0]]=[x[1],x[2],x[3]]);
obj = {};
naprawy.filter(x=>x[3] == "wymiana").map(x=>awarieObj[x[0]]?.at(0)).forEach(x=>x && (obj[x] = (obj[x] ?? 0) + 1));
console.log([["Komputer", "Liczba wymian"], ...Object.entries(obj).filter(x=>x[1]>=10)]);

let sekcje = {};
komputery.forEach(x=>{if(!sekcje[x[1]]) sekcje[x[1]] = []; sekcje[x[1]].push(x[0])});
sekcje = Object.entries(sekcje);
let day = awarie[0][2];
let buffer = [];
for(let a of awarie) {
    if(a[2] != day) {
        let sekcja = sekcje.find(([,x])=>x.every(y=>buffer.includes(y)));
        if(sekcja) {
            console.log(day, sekcja[0]);
            break;
        }
        day = a[2];
        buffer = [];
    }
    buffer.push(a[1]);
}

obj = {};
naprawy.forEach(x=>obj[x[0]] = x[1] + " " + x[2]);
let max = 0, index = 0;
for(let key in obj) {
    if((temp = new Date(obj[key]) - new Date(awarie[+key-1][2] + " " + awarie[+key-1][3])) > max) {
        max = temp;
        index = key;
    }
}
console.log(`Numer zgłoszenia: ${index}\nCzas wystąpienia awarii: ${awarie[+index-1][2]} ${awarie[+index-1][3]}\nCzas zakończenia ostatniej naprawy: ${obj[index]}`);

obj = new Set()
awarie.forEach(x => x[4] >= 8 && obj.add(x[1]))
console.log(komputery.length - obj.size)