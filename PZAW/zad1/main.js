const fs = require("fs");

const studenci = fs.readFileSync("studenci.txt",{encoding:'utf8', flag:'r'}).split("\n").slice(1, -1).map(x=>x.split("\t"))
const meldunek = fs.readFileSync("meldunek.txt",{encoding:'utf8', flag:'r'}).split("\n").slice(1, -1).map(x=>x.split("\t"))
const wypozyczenia =  fs.readFileSync("wypozyczenia.txt",{encoding:'utf8', flag:'r'}).split("\n").slice(1, -1).map(x=>x.split("\t"))

let obj = {};
wypozyczenia.forEach(x => obj[x[1]] = (obj[x[1]] ?? 0) + 1);
let pesel = Object.entries(obj).reduce((p,n)=>p[1] > n[1] ? p : n,[0,0])[0];
obj = wypozyczenia.filter(x=>x[1]==pesel).map(x=>x[2]);
pesel = studenci.find(x=>x[0] == pesel);
pesel = pesel[2] + " " + pesel[1];
console.log(pesel + "\n" + obj.join(", ") + "\n");

console.log((meldunek.length / new Set(meldunek.map(x=>x[1])).size).toPrecision(5) + "\n");

console.log([["Kobiety","Mężczyźni"],studenci.reduce((p,n)=>{
    if(+n[0][9] % 2 == 0) p[0]++;
    else p[1]++;
    return p;
},[0,0])].join("\n") + "\n")

obj = meldunek.map(x=>x[0])
console.log((pesel = studenci.filter(x=>!obj.includes(x[0]))).map(x=>x[1] + " " + x[2]).sort())

let count = 0;
obj = {"-1": pesel.map(x=>x[0])};
let obj2 = {};
meldunek.forEach(x=>{ if(!obj[x[1]]) obj[x[1]]=[]; obj[x[1]].push(x[0]) });
wypozyczenia.forEach(x=>{ if(!obj2[x[2]]) obj2[x[2]]=[]; obj2[x[2]].push(x[1]) })

for(let key in obj2) {
    for(let k in obj) {
        if(obj2[key].some(x=>obj[k].includes(x)))
            count++;
    }
}

console.log(count);